import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;


public class TreePanel extends JPanel
{
	private Tree tree;
	
	public TreePanel()
	{
		super();
		// tuy o PI/6 hay 12 de cay nho hY LON
		tree=new Tree(300,400,80,10,Math.PI/2, Math.PI/6, Color.green);
		
		this.setBackground(Color.white);
	}
	public void paintComponent(Graphics aBrush)
	{
		super.paintComponent(aBrush);
		Graphics2D betterBrush=(Graphics2D)aBrush;
		tree.draw(betterBrush);
	}

}
