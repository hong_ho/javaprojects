import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.geom.Line2D;


public class Tree 
{
	private Color color;
	private double length;
	private final int STROKE_WIDTH=2;
	private Line2D.Double firstLine;
	private Tree leftSubTree, rightSubTree;
	
	public Tree(double x, double y, double leng, double lengthChange,
				double angle, double angleChange, Color aColor )
	{
		super();
		color=aColor;
		length=leng;
		double endX=computerEndX(x, angle, length);// lay next endpoint
		double endY=computerEndY(y, angle, length);
		firstLine=new Line2D.Double(x,y, endX, endY);// ve line
		if(length <=3)// base case: line is short enough, basfallet de dung draw
		{
			leftSubTree=null;
			rightSubTree=null;
		}
		else // general case: not done yet
		{ // new dk dung thi ve tiep, diem dau cua new-line la diem cuoi cua old-line
			leftSubTree=new Tree(endX, endY, length-lengthChange, lengthChange,
								angle-angleChange, angleChange, aColor);
			rightSubTree=new Tree(endX, endY, length-lengthChange, lengthChange,
					angle+angleChange, angleChange, aColor);
		}
		
	}
	public void draw(Graphics2D aPaintBrush)
	{
		Color savedColor=aPaintBrush.getColor();
		aPaintBrush.setColor(color);
		Stroke savedStroke=aPaintBrush.getStroke();
		aPaintBrush.setStroke(new BasicStroke(STROKE_WIDTH));
		aPaintBrush.draw(firstLine);
		aPaintBrush.setStroke(savedStroke);
		aPaintBrush.setColor(savedColor);
		if(length<=3)
			return;
		else
		{
			leftSubTree.draw(aPaintBrush);
			rightSubTree.draw(aPaintBrush);
		}
	}
	public double computerEndX(double x, double angle, double length)
	{
		return x+length*Math.cos(angle);
	}
	public double computerEndY(double y, double angle, double length)
	{
		return y-length*Math.sin(angle);
	}

}
