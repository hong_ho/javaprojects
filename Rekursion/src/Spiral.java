/**
 * Chapter 12: Spiral.java
 * Uses recursion to create and then display a picture of a spiral.
 */
public class Spiral {
    private java.awt.Color _color; // attributes
    private double _length;
    private final int STROKE_WIDTH = 2;
    private java.awt.geom.Line2D.Double _firstLine; // components
    private Spiral _rest;

    public Spiral (double x, double y,
		   double length, double lengthChange,
		   double angle, double angleChange,
		   java.awt.Color aColor){
	super();
	_color = aColor;
	_length = length;
	double endX = computeEndX(x, angle, length);
	double endY = computeEndY(y, angle, length);
	_firstLine = new java.awt.geom.Line2D.Double (x, y,
						      endX, endY);
	if (_length <=3) { // base case: line is short enough
	    _rest = null; // (just makes default explicit)
	}
	else { // general case: not done yet
	    _rest = new Spiral(endX, endY, 
			       _length - lengthChange, lengthChange,
			       angle - angleChange, angleChange,
			       aColor);
	}
    }
    public double computeEndX (double x, double angle, double length) {
	return x + length * Math.cos(angle);
    }
    public double computeEndY (double y, double angle, double length) {
	return y - length * Math.sin(angle);
    }

    public void draw (java.awt.Graphics2D aPaintBrush) { 
	java.awt.Color savedColor = aPaintBrush.getColor();
	aPaintBrush.setColor(_color);
	java.awt.Stroke savedStroke = aPaintBrush.getStroke();
	aPaintBrush.setStroke(new java.awt.BasicStroke(STROKE_WIDTH));
	aPaintBrush.draw(_firstLine);
	aPaintBrush.setStroke(savedStroke);
	aPaintBrush.setColor(savedColor);
	if (_length <= 3) // base case: just drew shortest line
	    return;
	else // general case: not done yet
	    _rest.draw(aPaintBrush);
    }
}
