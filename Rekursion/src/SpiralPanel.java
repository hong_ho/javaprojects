import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;


public class SpiralPanel extends JPanel
{
	private Spiral spiral;
	
	public SpiralPanel()
	{
		super();
		spiral=new Spiral(150,200,80,4,Math.PI/2,Math.PI/4, Color.red);
		this.setBackground(Color.white);
	}
	public void paintComponent(Graphics aBrush)
	{
		super.paintComponent(aBrush);
		Graphics2D betterBrush=(Graphics2D)aBrush;
		spiral.draw(betterBrush);
	}
}
