import javax.swing.JFrame;


public class SpiralApp extends JFrame
{
	//private SpiralPanel s;
	
	public SpiralApp(String tittle)
	{
		super(tittle);
		this.setSize(600, 450);
		this.add(new SpiralPanel());
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
		this.setVisible(true);
		
	}

	public static void main(String[] args) {
		SpiralApp sa=new SpiralApp("Spiral");
	}

}
