
public class Person 
{
	private String namn;
	private int antalVaror;
	
	public Person(String name, int enAntalVaru)
	{
		namn=name;
		antalVaror=enAntalVaru;
	}
	public String getNamn()
	{
		return namn;
	}
	public void setNamn(String annatNamn)
	{
		namn=annatNamn;
	}
	public int getAntalVaru()
	{
		return antalVaror;
	}
	public void setAntalVaru(int annatTal)
	{
		antalVaror=annatTal;
	}
	public String toString()
	{
		String st= namn+" "+antalVaror;
		return st;
	}
}
