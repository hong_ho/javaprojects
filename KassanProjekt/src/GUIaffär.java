import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;


public class GUIaff�r extends JFrame implements ActionListener
{
	private Aff�r a;
	private Person p;
	private KassaK� k1, k2;
	
	private JList<Person> listAff�r,
							listK�1,
							listK�2;
	
	private JButton addButton = new JButton("Till Aff�r"), 
					flyttButton1=new JButton("Till K� 1"),
					flyttButton2=new JButton("Till K� 2"),
					betalButton1=new JButton("K�1 betalar"),
					betalButton2=new JButton("K�2 betalar"),
						betal=new JButton("L�mna Aff�r");
	private JTextField t1=new JTextField(15),
					   t2=new JTextField(12);
	private JLabel varuLabel=new JLabel("Varu: ",JLabel.CENTER),
					name=new JLabel("Name: ",JLabel.CENTER);
	
	
	public GUIaff�r(String tittle)
	{
		super(tittle);
		this.setSize(900, 270);
		
		
		a=new Aff�r();
		
		listAff�r=new JList<Person>(a.getList());
		listK�1=new JList<Person>(a.getListK1());
		listK�2=new JList<Person>(a.getListK2());
		
		JPanel pHead=new JPanel();pHead.setBackground(Color.white);
		pHead.add(name); pHead.add(t1);pHead.add(varuLabel);pHead.add(t2);
		pHead.add(addButton);
		
		JPanel pilar=new JPanel();pilar.setLayout(new GridLayout(2,1,0,10));
		pilar.add(flyttButton1);pilar.add(flyttButton2);
		
		
		JScrollPane s1=new JScrollPane(listK�1);
		JScrollPane s2=new JScrollPane(listK�2);
		JScrollPane s3=new JScrollPane(listAff�r);
		
		JPanel pK�1=new JPanel();
		pK�1.setLayout(new BorderLayout());
		pK�1.add(s1,BorderLayout.NORTH);
		pK�1.add(betalButton1, BorderLayout.SOUTH);
		
		JPanel pK�2=new JPanel();
		pK�2.setLayout(new BorderLayout());
		pK�2.add(s2,BorderLayout.NORTH);
		pK�2.add(betalButton2, BorderLayout.SOUTH);
		
		JPanel bodyPanel=new JPanel();bodyPanel.setBackground(Color.black);
		bodyPanel.add(s3);
		bodyPanel.add(pilar);
		bodyPanel.add(pK�1);
		bodyPanel.add(pK�2);
	
		addButton.addActionListener(this);
		flyttButton1.addActionListener(this);
		flyttButton2.addActionListener(this);
		betalButton1.addActionListener(this);
		betalButton2.addActionListener(this);
		betal.addActionListener(this);
		
	
		this.add(pHead, BorderLayout.NORTH);
		this.add(bodyPanel, BorderLayout.CENTER);
		this.add(betal,BorderLayout.SOUTH);
		this.setVisible(true);
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{	int v;
		
		if(e.getSource()==addButton)
		{try
			{String h=t2.getText();
				v=Integer.parseInt(h);
		}catch (Exception h)
			{
				JOptionPane.showMessageDialog(null, "Mata in heltal");
				//System.out.println("heltal"); 
				return;
			}
		p=new Person(t1.getText(),v );

			a.add(p);
		}
		else if(e.getSource()==flyttButton1)
		{
			Person i=listAff�r.getSelectedValue();
			a.flytta(i,1);
		}
		else if(e.getSource()==flyttButton2)
		{
			Person i=listAff�r.getSelectedValue();
			a.flytta(i,2);
		}
		else if(e.getSource()==betalButton1)
		{
			a.getListK1().remove(0);
		}
		else if(e.getSource()==betalButton2)
		{
			a.getListK2().remove(0);
		}
		else if(e.getSource()==betal)
			System.exit(0);
		
	}

	
}
