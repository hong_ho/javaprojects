import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class LuffarschacksView extends JFrame implements Observer
{
	private JPanel rutaPanel, infPanel;
	private JButton resetButton, stopButton;
	private JTextField spel1, spel2;
	private JButton[][] ruta;
	private static  final int COL=3;
	private  static final int ROW=3;
	private JLabel sp1, sp2;
	
	
	/*public JButton[][] getRuta() {
		return ruta;
	}

	public void setRuta(JButton[][] ruta) {
		this.ruta = ruta;
	}*/

	public LuffarschacksView(LuffarScharkControl lc)
	{
		this.setTitle("Luffarschacksspel");
		this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
		this.setSize(300, 350);
		this.setLayout(new BorderLayout());
				
		// creates row, col
		rutaPanel=new JPanel(); rutaPanel.setBackground(Color.black);
		rutaPanel.setLayout(new GridLayout(3,3,0,0));
		
		
		skapaRutor(lc);
		
		infPanel=new JPanel(); infPanel.setLayout(new BorderLayout());
		JPanel p1=new JPanel();p1.setBackground(Color.black);
		sp1=new JLabel("Spelare X:");sp1.setForeground(Color.white);
		spel1=new JTextField(15); 
		p1.add(sp1);
		p1.add(spel1);
		
		JPanel p2=new JPanel();p2.setBackground(Color.black);
		sp2=new JLabel("Spelare O:");sp2.setForeground(Color.white);
		spel2=new JTextField(15);spel2.addActionListener(lc);spel2.setActionCommand("sp1");
		p2.add(sp2);
		p2.add(spel2);
		
		JPanel p3=new JPanel();p3.setBackground(Color.black);
		resetButton=new JButton("Reset"); resetButton.addActionListener(lc);
		stopButton=new JButton("Stopp");stopButton.addActionListener(lc);
		resetButton.setActionCommand("reset");
		stopButton.setActionCommand("stopp");
		p3.add(resetButton);
		p3.add(stopButton);
		
		infPanel.add(p1, BorderLayout.NORTH);
		infPanel.add(p2, BorderLayout.CENTER);
		infPanel.add(p3, BorderLayout.SOUTH);	
		
		
		this.add(infPanel, BorderLayout.NORTH);
		this.add(rutaPanel, BorderLayout.CENTER);
		this.setVisible(true);
	}

	public void skapaRutor(LuffarScharkControl lc)
	{
		ruta=new Ruta[ROW][COL];
		for(int r=0;r<ROW;r++)
			{for(int c=0; c<COL;c++)
				{
				ruta[r][c]=new Ruta(r, c);
				ruta[r][c].setHorizontalAlignment(JButton.CENTER);
				ruta[r][c].setFont(new Font("Arial", Font.BOLD, 25));
				ruta[r][c].addActionListener(lc);
				rutaPanel.add(ruta[r][c]);		
				ruta[r][c].setActionCommand(Integer.toString(r) + " " +  Integer.toString(c));
				ruta[r][c].setEnabled(true);
				}
			}
	}
	@Override
	public void update(Observable o, Object arg)
	{
		LuffarScharkModel m = (LuffarScharkModel)o;
		
		int[][] gameB = m.getGameB();
		if(spel1.getText().equals("")|| spel2.getText().equals(""))
		{
			JOptionPane.showMessageDialog(null,"Mata in spelarnas name.");
		}
		else
		{
			for (int row = 0; row < 3; row++)
				for (int col=0; col < 3; col ++)
				{
					//System.out.println(gameB[row][col])
					if (!(gameB[row][col] == 0) )
					{
						if (((Ruta)this.ruta[row][col]).isEmpty() )
						{
							((Ruta)this.ruta[row][col]).setSymbol(m.getSpelare());
							((Ruta)this.ruta[row][col]).setEmpty(false);
							((Ruta)this.ruta[row][col]).setEnabled(false);
							
						}	
											
					}
					
				} 
		}
		
			if(m.vinn()>0)
			
			{
				if(m.getSpelare()==1)
					{
						String st=Integer.toString(m.getSpelare()); st=spel1.getText();
						JOptionPane.showMessageDialog(null, st + " vinner. Grattis! :-)");
						m.reset();
					}
				else
					{
						String s2=Integer.toString(m.getSpelare()); s2=spel2.getText();
						JOptionPane.showMessageDialog(null, s2 + " vinner. Grattis! :-)");
						m.reset();
					}
			}
			
		else if(m.fullBoard())
		{
			JOptionPane.showMessageDialog(null, "Spelplanen �r uppfyllt.");
			
		}
		if(!m.isPlaying())
		{
			spel1.setText("");
			spel2.setText("");
			for (int row = 0; row < 3; row++){
				for (int col=0; col < 3; col ++)
				{		
					((Ruta)this.ruta[row][col]).setText("");;
					((Ruta)this.ruta[row][col]).setEmpty(true);
					((Ruta)this.ruta[row][col]).setEnabled(true);
				}
			}
			m.setPlaying(true);
			
		}		
	}
}
