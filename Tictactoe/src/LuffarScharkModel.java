import java.util.Observable;




public class LuffarScharkModel extends Observable
{
	private boolean playing=true;
	int win=0;
	private boolean uppFyll=true;
	private int spelare;
	private int[][] gameB= new int[3][3];
	
	public LuffarScharkModel()
	{
		this.setSpelare(1);
		for ( int i = 0; i < 3; i ++ )
			for ( int j = 0; j < 3; j++ )
			{
				this.gameB[i][j] = 0;
			}
	}
	public void stopp()
	{
		playing =false;
		
		System.exit(0);
	}
	public void reset()
	{
		/*
		 * {
			 playing =false;
			
			for ( int i = 0; i < 3; i ++ )
				for ( int j = 0; j < 3; j++ )
					this.gameB[i][j] = 0;
				
			//this.checkTur();
			win = 0;
			
				// n�r �ndra v�rde s� anropas
				setChanged();
				notifyObservers();
			}
		 */
		playing =false;
		boolean changed = false;
		for ( int i = 0; i < 3; i ++ )
			for ( int j = 0; j < 3; j++ )
			{
				if(this.gameB[i][j] != 0)
					changed = true;
				this.gameB[i][j] = 0;
			}
		
		win = 0;
		if(changed){
			// n�r �ndra v�rde s� anropas
			setChanged();
			notifyObservers();
		}
	}
	public int getSpelare()
	{
		return spelare;
	}
	public void setSpelare(int s)
	{
		this.spelare=s;
	}
	
	public void checkTur()
	{			
		if(this.getSpelare() == 1 )
			this.setSpelare(2);
		else 
			this.setSpelare(1);		
	}
	public boolean isPlaying()
	{
		return playing;
	}
	public void setPlaying(boolean b) {
		playing = true;
		
	}
	
	public void play(int row, int col)
	{
		if (this.gameB[row][col] == 0 )
		{
			this.gameB[row][col] = this.getSpelare();
			this.isWin(this.spelare);			
			this.fullBoard();			
			setChanged();
			notifyObservers();
			this.checkTur();
		}				
	}
	public int[][] getGameB() {
		return gameB;
	}
	public void setGameB(int[][] gameB) {
		this.gameB = gameB;
	}
	public int vinn()
	{
		return win;
	}
	public void isWin(int spelare)
	{
		if(gameB[0][0] == spelare && gameB[1][0] == spelare && gameB[2][0] == spelare)
			win = spelare;
		if(gameB[0][1] == spelare && gameB[1][1] == spelare && gameB[2][1] == spelare)
			win = spelare;
		if(gameB[0][2] == spelare && gameB[1][2] == spelare && gameB[2][2] == spelare)
			win = spelare;
		if(gameB[0][0] == spelare && gameB[0][1] == spelare && gameB[0][2] == spelare)
			win = spelare;
		if(gameB[1][0] == spelare && gameB[1][1] == spelare && gameB[1][2] == spelare)
			win = spelare;
		if(gameB[2][0] == spelare && gameB[2][1] == spelare && gameB[2][2] == spelare)
			win = spelare;
		if(gameB[0][0] == spelare && gameB[1][1] == spelare && gameB[2][2] == spelare)
			win = spelare;
		if(gameB[2][0] == spelare && gameB[1][1] == spelare && gameB[0][2] == spelare)
			win = spelare;
	}
    public boolean fullBoard()
    {
        for(int line=0 ; line<3 ; line++)
            for(int column=0 ; column<3 ; column++)
                if( gameB[line][column]==0 )
                    return false;
        return true;
    }
}
