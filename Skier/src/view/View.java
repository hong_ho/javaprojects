package view;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import controler.Controler;
import model.Record;

public class View {	
	private Controler c;
	private BufferedReader in;
	public View(Controler con)
	{
		in=new BufferedReader(new InputStreamReader(System.in));
		this.c=con;
	}
	public void runUi()
	{
		String slask="100";
		try{
			while(Integer.parseInt(slask)!=0)
			{
				System.out.println("__________Menu___________");
				System.out.println("0: Quit.");
				System.out.println("1: Add Record.");
				System.out.println("2: Search Record.");
				System.out.println("3: Show Prev Record.");
				System.out.println("4: Show Next Record.");
				System.out.println("5: Remove Record.");
			
				slask=in.readLine();
				c.handleEvent(Integer.parseInt(slask));
			}
		}
		catch(Exception e)
		{
			System.out.println("Unexpected events in View.runUI exiting");
            System.exit(1);
        
		}
	}
	public void viewEdit(Record r)
	{
		try{
			System.out.println("Enter personnumber: ");
			r.setPrn(in.readLine());
			System.out.println("Enter name: ");
			r.setname(in.readLine());
			System.out.println("Enter club: ");
			r.setClub(in.readLine());
		}
		catch (Exception e) {
        System.out.println("Unexpected events in View.viewEdit exiting");
        System.exit(1);
    }
		
	}
	public void viewCurr(Record r)
	{
		System.out.println("Prn: "+r.getPnr());
		System.out.println("Name: "+r.getName());
		System.out.println("Club: "+r.getClub());
		System.out.println("\n\n");
	}
	public String viewSearch()
	{
		String slask="";
	
		try{
			System.out.println("Enter prn: \n");
			slask=in.readLine();
		}
		catch (Exception e) {
        System.out.println("Unexpected events in View.viewEdit exiting");
        System.exit(1);}
		return slask;
	}
}
