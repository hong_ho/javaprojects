package controler;

import model.Record;
import model.RecordManager;
import view.View;

public class Controler {
	private Record curr;
	private RecordManager rm;
	private View ui;
	private String sok="";
	
	public Controler()
	{
		rm=new RecordManager();
		rm.openFile("anmal.dat");
		ui=new View(this);
		ui.runUi();
	}
	
	public void handleEvent(int val)
	{
		if(val==0)
		{
			rm.closeFile();
			System.exit(0);
		}
		if(val==1)//createRec
		{
			rm.createRecord();
			ui.viewEdit(curr);
			rm.addRecord(curr);
		}
		if (val==2) {
			   sok=ui.viewSearch();
			   curr=rm.searchRecord(sok);
			}
		if (val==3) {
			   curr=rm.nextRecord();
	                   if (curr==null) {
	                      curr = rm.lastRecord();
	                   }
	                }
		ui.viewCurr(curr);
		
	}
	public static void main(String [] args)
	{
		new Controler();
	}

}
