package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.LinkedList;

public class RecordManager {
	private LinkedList v;
	private int index=0;
	private Record curr;
	private File f;
	
	public void openFile(String s)
	{
		f=new File(s);
		if(f.exists())
		{
			try{
				ObjectInputStream i=new ObjectInputStream(new FileInputStream(f));
				v=(LinkedList)i.readObject();
				i.close();
			}
			catch(Exception e)
			{
				System.out.println("Fatal Error:The file does not contain data as expected....exiting");
				System.exit(1);
			}
		}
		else
		{
			v=new LinkedList();
			System.out.println("Warning:  File does not exist...creating file");

		}
	}
public void closeFile()
{
	try
	{
		ObjectOutputStream i=new ObjectOutputStream(new FileOutputStream(f));
		i.writeObject(v);
		i.close();
		System.out.println("Message:  Vector saved to file");
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:Failue saving file....exiting");
		System.exit(1);
	}
}
public void addRecord(Record r)
{
	try
	{
		v.addLast(r);
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:The file has not been opened so exiting (add)");
		System.exit(1);
	}
}
public void removeRecord(Record r)
{
	try
	{
		v.remove(r);
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:The file has not been opened or less than two records exists so exiting (remove)");
		System.exit(1);
	}
}
public Record firstRecord()
{
	try
	{
		curr=(Record)v.get(0);
		return curr;
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:The file has not been opened or no records exists so exiting (first)");
		System.exit(1);
		return null;
	}
}
public Record lastRecord()
{
	try
	{
		curr=(Record)v.getLast();
		index=v.size()-1;
		return curr;
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:The file has not been opened or no records exists so exiting (last)");
		System.exit(1);
		return null;
	}
}
public Record nextRecord()
{
	index++;
	try
	{
		int tmp=v.size();
		if(index==tmp)
		{
			index--;
			return null;
		}
		else
		{
			curr=(Record)v.get(index);
			return curr;
		}		
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:The file has not been opened or no records exists so exiting (next)");
		System.exit(1);
		return null;
	}	
}
public Record prevRecord()
{
	index--;
	if(index<-1)
	{
		index=0;
		return null;
	}
	
		try
		{
			if(index==0)
				return null;
			else
			{
				curr=(Record)v.get(index);
				return curr;
			}
		}
		catch(Exception e)
		{
			System.out.println("Fatal Error:The file has not been opened or no records exists so exiting (next)");
			System.exit(1);
			return null;
		}	
		
	}
public Record createRecord()
{
	return new Record();
}
public Record searchRecord(String s)
{
	try
	{
		Iterator i=v.iterator();
		Record tmp=null;
		index=0;
		while(i.hasNext())//return true om finns mer obj in v
		{
			curr=(Record)i.next();// l�s vidare
			if(s.equals(curr.getPnr()))//testa
			{
				index=v.indexOf(curr);
				tmp=curr;
			}
		}
		return curr;
	}
	catch(Exception e)
	{
		System.out.println("Fatal Error:The file has not been opened or no records exists so exiting (searchs)");
		System.exit(1);
		return null;
	}
}

}
