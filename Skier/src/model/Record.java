package model;

import java.io.Serializable;

public class Record implements Serializable {
	private String pnr="";
	private String name="";
	private String club="";
	
	public String getPnr()
	{
		return this.pnr;
	}
	public String getName()
	{
		return this.name;
	}
	public String getClub()
	{
		return this.club;
	}
	public void setPrn(String p)
	{
		this.pnr=p;
	}
	
	public void setname(String n)
	{
		this.name=n;
	}
	public void setClub(String c)
	{
		this.club=c;
	}
	

}
