package ControllerPackage;


import ModelPackage.FileManagement;
import ModelPackage.NotepadModels;
import View.ViewGui;
import View.ViewInterface;
//import ViewConsole.View;

public class NotepadController
{
	private ViewFactory vf;
	private NotepadModels m;
	private FileManagement f;
	private ViewInterface v;
	
	
	public NotepadController()
	{
		f=new FileManagement();
		m=new NotepadModels();
		vf=new ViewFactory(this);
		v=vf.createView();
		v.setUp();
		
	}
	public void handleEvent(int choose) {
		if (choose==0) {
		   boolean changed=v.isChanged();
			if(changed)
			{
				v.failMsg();
				
				if(v.getAnswer()==0)
				{
					if(v.getName().equals("*Notepad"))
					{	
						saveAs();
					}
					else
						save();
					
					m.exit();
				}
				if(v.getAnswer()==1)
				{
					m.exit();
				}
				if(v.getAnswer()==2)
				{
					;
				}
			}
			else
			{				
				m.exit();
			}	
		}
		if (choose==1) //new
		{
			
			boolean changed=v.isChanged();
			if(changed)
			{				
				v.failMsg();
				
				if(v.getAnswer()==0)
				{
					if(v.getName().equals("*Notepad"))
					{	
						saveAs();
					}
					else
						save();
					
					newFile();
				}
				if(v.getAnswer()==1)
				{
					newFile();
				}
				if(v.getAnswer()==2)
				{
					;
				}
			}
			else
			{				
				newFile();
			}					
		}
		if (choose==2) //open
		{
			
			boolean changed=v.isChanged();
			if(changed)
			{
				v.failMsg();
				if(v.getAnswer()==0)
				{
					if(v.getName().equals("*Notepad"))
					{	
						saveAs();
					}
					else
						save();
					newFile();
					open();					
				}
				if(v.getAnswer()==1)
				{
					newFile();
					open();
				}
				if(v.getAnswer()==2)
				{
					;
				}
			}
			else
			{
				//newFile();
				open();
			}	
		}
		if (choose==3) //save -kolla mer
		{
			if(v.getName().equals("*Notepad")||v.getName().equals("Notepad"))
			{	
				saveAs();
			}
			else
				save();
			
		}
		if (choose==4) //saveas
		{
			saveAs();
		}	
		if (choose==5) //copy
		{
			copyText();
		}
		if (choose==6) //cut
		{
			cutText();
		}
		if (choose==7) //paste
		{
			pasteText();
		}
		
		
	}
	private void newFile()
	{
		String t=m.newFile();
		v.newFile(t);
	}
	private void open()
	{
		//String text="";
		//if(v.choiceOpen()==0)
		String fileN=v.fileNameOpen();
		String text=f.readFile(fileN);
		v.open(text);
		v.setName(fileN);
	}	
	private void save()
	{
		String t=v.contentText();
	//	System.out.println(t);
		//String fileN=v.fileName();	
		//String fileN=v.getName();	
		//System.out.println(fileN);
		String fileN=v.saveFilename();
		f.saveAsFile(t, fileN);
		v.setName(fileN);
	}
	private void saveAs()
	{
		String t=v.contentText();
		String filename=v.fileName();	
		//String filename=v.fileName1();
		f.saveAsFile(t, filename);
		v.setName(filename);
	}
	private void copyText()
	{
		String copytext=v.copyText();
		m.setText(copytext);
	}
	private void pasteText()
	{
		String pastetext=m.getText();
		v.pasteText(pastetext);
	}
	private void cutText()
	{
		String cuttext=v.cutText();
		m.setText(cuttext);
	}	
}
