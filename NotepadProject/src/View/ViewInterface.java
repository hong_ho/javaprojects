package View;

public interface ViewInterface {
	public String contentText();	
	public String fileName();	
	public String fileNameOpen();	
	public void open(String text);
	public String getName();	
	public void setName(String fn);	
	public void newFile(String s);	
	public boolean isChanged();	
	public void failMsg();	
	public int getAnswer();	
	public void checkButton(int choose);	
	public String copyText();	
	public String cutText();	
	public void pasteText(String text);
	public void setUp();	
	public String saveFilename();
	
}
