package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OpenListener implements ActionListener{

	private ViewGui v;
	public OpenListener(ViewGui v) {
		this.v=v;
		}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(2);
		
	}
	

}
