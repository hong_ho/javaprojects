package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NewFileListener implements ActionListener {
	
	private ViewGui v;
	
	public NewFileListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(1);	
		System.out.println(v.getName());
	}

}
