package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SaveAsListener implements ActionListener{
	private ViewGui v;
	public SaveAsListener(ViewGui v)
	{
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(4);
		
	}

}
