package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BackListener implements ActionListener {

	private ViewGui v;
	public BackListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(10);
		
	}

}
