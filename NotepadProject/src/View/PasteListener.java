package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PasteListener implements ActionListener{

	private ViewGui v;
	public PasteListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(7);
		
	}

}
