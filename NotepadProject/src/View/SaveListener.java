package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SaveListener implements ActionListener{
	private ViewGui v;
	public SaveListener(ViewGui v) {		 
		this.v=v;
		}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(3);
		
	}

}
