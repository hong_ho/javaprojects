package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ExitListener implements ActionListener{

	private ViewGui v;
	public ExitListener(ViewGui v) {
		this.v=v;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		v.checkButton(0);
		
	}

}
