import java.awt.*;

import wheels.users.Ellipse;

public class Ball
{
		private Ellipse boll;
		private int x=0,y=0,radius, dx=4,dy=4;
		private Color color=Color.blue;
		
	public Ball(int x, int y, int radius, Color color)
		{
			boll=new Ellipse();
			this.x=x;
			this.y=y;
			this.radius=radius;
			this.color=color;
			boll.setSize(radius, radius);
			boll.setColor(color);
			boll.setLocation(x,y);
		}
	public void setPosition(int x, int y)
	{
		this.x=x;
		this.y=y;
		boll.setLocation(x, y);
	}
	
		public void setColor(Color c)
		{
			boll.setColor(c);
		}
		public int getBollx()
		{
			return x;
		}
		public int getBolly()
		{
				return y;
		}
		public int getRadius()
		{
			return radius;
		}
		public void setBollX(int x)
		{
			this.x=x;
			
		}
		public void setBollY(int y)
		{
			this.y=y;
			
		}
		
		public void move(int dx, int dy)
		{
	   		
	   		this.x=x+dx;
			this.y=y+dy;
			boll.setLocation(x, y);
		}
   	public void move(Bat p)
		{	   		
	   		this.x=x+dx;
			this.y=y+dy;
			// check wall
			if(x<0)
				dx=-dx;
			if(x> 800)
				dx=-dx;
			if(y<0)
				dy=-dy;
			if(y>600) // beh�ver inte starta om
				dy=-dy;
			// checkpaddle
			if(x>p.getSlagTr�dx() && x < p.getSlagTr�dx()+p.getSlagTr�dw())
				
			{
				if(y+radius>=p.getSlagTr�dy() && y+radius<= p.getSlagTr�dy()+p.getSlagTr�dh())
				
					{
					dy=-dy;
					}
			}		
			boll.setLocation(x, y);
		}
   	public boolean checkBrick(Brick br)
   	{
   		if(br==null)
   			return false;
   		if((x>br.getBrickx()) && (x<br.getBrickx()+br.getBrickw()))
			{
			if(y<=br.getBricky()+br.getBrickh() && y>=br.getBricky())
				{	
					dy=-dy;
					return true;
				}
			}
   		return false;
   	}
}
