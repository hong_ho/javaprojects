import java.awt.Color;

import wheels.users.Rectangle;


public class Brick
{
	private Rectangle r;
	private int x=0,y=0;
	private int w=78,h=28;
	private int po�ng=0;
	private Color color;
	private int hit=0;
	public Brick(int x, int y)
	{
		this.x=x;
		this.y=y;
		r=new Rectangle();
		r.setSize(w, h);
		r.setColor(Color.blue);
		r.setFrameColor(Color.black);
		r.setLocation(x, y);
				
	}
	public void setHit(int h)
	{
		hit+=h;
	}
	public int getHit()
	{
		return hit;
	}
	
	public void setColor(Color c)
	{
		r.setColor(c);
	}
	public Color getColor()
	{
		return color;
	}
	public int getBrickx()
	{
		return x;
	}
	public int getBricky()
	{
		return y;
	}
	public int getBrickw()
	{
		return w;
	}
	public int getBrickh()
	{
		return h;
	}
	public void setPo�ng(int p)
	{
		po�ng=p;
	}
	public int getPo�ng()
	{
		return po�ng;
	}
	public void taBort()
	{
		r.setLocation(900, 600);
		
	}
	
}
