import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseMotionListener;
import java.util.Random;
import java.awt.geom.*;

import javax.swing.JLabel;

import wheels.users.Frame;

public class GameBoard extends Frame
{
	private Ball b;
	private Bat bar;
	private Brick[] brickor=new Brick[20];	
	private int x=0,y=0;
	private int po=0;	
	private int dx=10, dy=10;
	private int nbrick=0;	
	private int sum=0;
	private JLabel lp;

	public GameBoard() throws InterruptedException
	{	
		b=new Ball(0,0,20,Color.blue);
		b.setPosition(400, 200);
		bar=new Bat(50,450);		
		lp=new JLabel("Po�ng: ", JLabel.CENTER);
		this.addElementRow(lp);
		
		//skapaBrickor
		for(int i=0; i<brickor.length ;i++)
		{
			brickor[i]=new Brick(x,y);				
			x=x+80;
			if((i+1)%10==0)
			{
				x=0;
				y=y+30;
			}
					
		Random r=new Random();
		int n=r.nextInt(3)+1;				
			if(n==1)
				{
					brickor[i].setPo�ng(n);
					brickor[i].setHit(1);
					brickor[i].setColor(Color.red);					
				}
			else if(n==2)
				{
					brickor[i].setPo�ng(n);
					brickor[i].setHit(1);
					brickor[i].setColor(Color.green);
				}	
			else
			{
				brickor[i].setColor(Color.blue);
				brickor[i].setPo�ng(1);
				brickor[i].setHit(3);
				
			}
		}	
		
		while(true)
		{	
			b.move(bar);	
			for(int i=0;i<brickor.length;i++)
			{	
				if(b.checkBrick(brickor[i]))
				{
					brickor[i].setHit(-1);
					
					if(brickor[i].getHit() == 0)
					{
						po=brickor[i].getPo�ng();
						brickor[i].taBort();
						brickor[i]=null;
					}
					sum=sum+po;				
				}					
			}
			lp.setText("Po�ng: "+ sum);
			Thread.sleep(20);		
		}			
	}
	
}
	

	

