
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
// java 2D spel - youtube - Java 2D Spel Svenska
public class PickAndStick extends JFrame 
{
	PickStickAnimationMouth animationImage;
	//Image chewMan; thay hinh bang animationImage
	Rectangle hero=new Rectangle(0,0,50,50);
	int score=0;
	int gX,gY;
	long startTime, endTime;
	int seconds;
	int x, y;
	boolean hit=false;
	ArrayList<Gobbler> gobs=new ArrayList<Gobbler>();
	
	public void loadImage()
	{
		Image chewMan=new ImageIcon("chewMouth.png").getImage();// bien ImageIcon thanh Image
		Image pacMan=new ImageIcon("pacMouth.png").getImage();// bien ImageIcon thanh Image// de lam img dong
		
		animationImage=new PickStickAnimationMouth();
		
		animationImage.addFrame(chewMan, 200);
		animationImage.addFrame(pacMan, 150);
		animationImage.addFrame(chewMan, 200);
		animationImage.addFrame(pacMan, 150);
		animationImage.addFrame(chewMan, 200);
		animationImage.addFrame(pacMan, 150);
		animationImage.addFrame(chewMan, 200);
		animationImage.addFrame(pacMan, 150);
	}
	public PickAndStick()
	{
		
		this.setTitle("Pick and Stick");
		this.setSize(400, 400);
		this.setDefaultCloseOperation(3);
		this.setVisible(true);
		this.setResizable(false);
		
		this.addKeyListener(new Input());
		addGobbler(10);
	
	//System.currentTimeMillis();
		hero.x=50;
		hero.y=50;
		
		loadImage();
		startTime=System.currentTimeMillis();
		animationLoop();
		
	}
	public void addGobbler(int amount)
	{
		for(int i=0;i<amount;i++)
			gobs.add(new Gobbler());
	}
	public static void main(String[] args)
	{
		new PickAndStick();
	}
	//neu chi o paint() thi thay 2 re nhung repanit() thi ko thay rec red
	//nen doi paint() thanh draw va them paint()
	
	/*public void paint(Graphics g)
	{
		//if(hero.intersects(gX,gY,20,20))
		if(hit)
		{	
			endTime=System.currentTimeMillis();
			seconds=(int)(endTime - startTime) / 1000;
			//moveGobbler();
			score += 10-seconds;
			startTime=endTime;
			hit=false;
		}
		Image offScreen=createImage(getWidth(),getHeight());
		draw(offScreen.getGraphics());//bild offscreen ta in , ko fai JFrame
		g.drawImage(offScreen, 0, 0, null);// null la ko co observer
	}*/
	// khi them animationImage thi thay paint thanh updateScreen de khoi bao loi
	public void updateScreen()
	{
		Graphics g=getGraphics();// kom fr�n komponent
		//if(hero.intersects(gX,gY,20,20))
		if(hit)
		{	
			endTime=System.currentTimeMillis();
			seconds=(int)(endTime - startTime) / 1000;
			//moveGobbler();
			score += 10-seconds;
			startTime=endTime;
			hit=false;
		}
		Image offScreen=createImage(getWidth(),getHeight());
		draw(offScreen.getGraphics());//bild offscreen ta in , ko fai JFrame
		g.drawImage(offScreen, 0, 0, null);// null la ko co observer
	}
	
	public void draw(Graphics g)
	{
		g.setColor(Color.GREEN);
		g.fillRect(0, 0, 400, 400);
		
		g.setColor(Color.YELLOW);
	//	g.fillOval(gX, gY, 20, 20);
		for(int i=0; i<gobs.size();i++)
		{	int x=gobs.get(i).getX();
		int y=gobs.get(i).getY();
		int height=gobs.get(i).rect.height;
		int width=gobs.get(i).rect.width;
		g.fillOval(x, y, width, height);
		gobs.get(i).collision();
		}
		/*g.setColor(Color.RED);
		g.fillRect(hero.x, hero.y, 50, 50);*/// thay vao do la image
		//g.drawImage(chewMan, hero.x, hero.y, null); vi thay animationImage
		g.drawImage(animationImage.getImage(), hero.x, hero.y, null);
		
		g.setColor(Color.BLACK);
		g.drawString("Score: "+ score, 25, 55);
		
	//	repaint(); khi thay ten updateScreen thi bo paint - ko thi no nhay hinh
	}
	
	private void setX(int x)
	{
		this.x=x;
	}
	private void setY(int y)
	{
		this.y=y;
	}
	public void update()
	{
		hero.x +=x;
		hero.y +=y;
		
		if(hero.x<0)
			hero.x=0;
		else if(hero.x>350)
			hero.x=350;
		
		if(hero.y<30)
			hero.y=30;
		else if(hero.y>350)
			hero.y=350;
		
		
	}
	public void animationLoop()
	{
		/*try
		{
			while(true)
			{
				update();
				Thread.sleep(10);
			}
		}catch(InterruptedException e){
			
		}*/
		// khi thay animationImage
		try
		{
			long currentTime=System.currentTimeMillis();
			long animationTime;
			while(true)
			{
				animationTime=System.currentTimeMillis()-currentTime;
				currentTime=animationTime;
				
				animationImage.update(currentTime);
				
				update();
				updateScreen();
				Thread.sleep(20);
			}
		}catch(InterruptedException e){
			
		}
	}
	private class Input implements KeyListener
	{

		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode()==e.VK_UP)
			{
				setY(-5);
				/*if(hero.y>30)
					hero.y -=7;
				else
					hero.y=30;*/
			}
			if(e.getKeyCode()==e.VK_DOWN)
			{
				setY(5);
				/*if(hero.y<350)
					hero.y +=7;
				else
					hero.y=350;*/
			}
			if(e.getKeyCode()==e.VK_LEFT)
				{
				setX(-5);
//					if(hero.x>0)
//						hero.x -=7;
//					else
//						hero.x=0;
				}
			if(e.getKeyCode()==e.VK_RIGHT)
			{
				setX(5);
//				if(hero.x<350)
//					hero.x +=7;
//				else
//					hero.x=350;
			}
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			if(e.getKeyCode()==e.VK_UP)
			{
				setY(0);
				
			}
			if(e.getKeyCode()==e.VK_DOWN)
			{
				setY(0);
				
			}
			if(e.getKeyCode()==e.VK_LEFT)
				{
				setX(0);
//					
				}
			if(e.getKeyCode()==e.VK_RIGHT)
			{
				setX(0);
//				;
			}
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
//	private void moveGobbler()
//	{
//		gX=(int) (Math.random() * 400);
//		gY=(int) (Math.random() * 400);
//		
//		if(gY<30)
//			gY=30;
//		else if(gY>380)
//			gY=380;
//		
//		if(gX>380)
//			gX=380;
//		
//	}
	private class Gobbler
	{
		Rectangle rect;
		
		public Gobbler()
		{
			rect=new Rectangle(0,0,20,20);
			moveGobbler();
		}
		public int getX()
		{
			return rect.x;
		}
		public int getY()
		{
			return rect.y;
		}
		private void moveGobbler()
		{
			rect.x=(int) (Math.random() * 400);
			rect.y=(int) (Math.random() * 400);
			
			if(rect.y<30)
				rect.y=30;
			else if(rect.y>380)
				rect.y=380;
			
			if(rect.x>380)
				rect.x=380;
			
		}
		public void collision()
		{
			if(rect.intersects(hero))
			{
				moveGobbler();
				hit=true;
			}
		}
	}
	
}
