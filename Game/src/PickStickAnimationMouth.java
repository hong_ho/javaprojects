import java.awt.Image;
import java.util.ArrayList;

class PickStickAnimationMouth
{
	/*oms-1000ms
	4 bilder- 250ms per bild
	b�rja b1 0ms-250, b2 250-500ms */
	
	private ArrayList<Frame> frames=new ArrayList<Frame>();
	
	private int index;// vilken bilde kolla p�
	private long point; //kolla exakt vilken millisecond, vad vi �e n�nstan
	private long totalAnimationTime;
	
	public void addFrame(Image img, long time)
	{
		totalAnimationTime +=time;// moi lan + them 1 bild thi + totaltime len
		//them b3 thi total tang fr�n 500-750
		
		frames.add(new Frame(img, totalAnimationTime));
		
	}
	public Image getImage()
	{
		if(frames.size()<1)
			return null;
		return frames.get(index).img;
	}
	public void update(long time)
	{
		point +=time;
		if(point>=totalAnimationTime)
		{
			point=point%totalAnimationTime;
			index=0;
		}
		// bild1:0-50ms b2: 50-75ms b3: 73-125.point=135ms-->ngoai gr�nsen
		while(point>frames.get(index).stopTime)
		{
			index++;
		}
	}
	private class Frame
	{
		public Image img;
		public long stopTime;
		
		public Frame(Image image, long time)
		{
			img=image;
			stopTime=time;
		}
	}
}