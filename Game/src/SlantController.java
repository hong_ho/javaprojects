import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class SlantController implements ActionListener
{
	private SlantModel M;
	public SlantController(SlantModel enModell)
	{
		M = enModell;
	}
	public void actionPerformed(ActionEvent arg0)
	{
		M.singlaSlant();
	}
}
