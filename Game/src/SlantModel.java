import java.util.Observable;
import java.util.Random;


public class SlantModel extends Observable
{
	public enum l�gen {KRONA, KLAVE, H�GKANT, ODEFINIERAT};
	private l�gen l�ge;
	Random r;
	public SlantModel()
	{
		l�ge = l�gen.ODEFINIERAT;
		r = new Random();
	}
	public void singlaSlant()
	{
		int slumpv�rde = r.nextInt(200);
		if(slumpv�rde == 0)
			l�ge = l�gen.H�GKANT;
		else if(slumpv�rde > 0 && slumpv�rde < 100)
			l�ge = l�gen.KLAVE;
		else
			l�ge = l�gen.KRONA;
		setChanged();
		notifyObservers(l�ge);
	}
	public l�gen getL�ge()
	{
		return l�ge;
	}
}
