import java.awt.Graphics;


public class Paddle 
{
	private int x=0,y=0, width=120, height=16;
	public boolean goingLeft=false;
	public boolean goingRight=false;
	
	public Paddle(int x, int y)
	{
		this.x=x;
		this.y=y;
		
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getW()
	{
		return width;
	}
	public int getH()
	{
		return height;
	}
	public void move(int maxWidth)
	{
		
		if(goingLeft)
			x -=4;
		if(goingRight)
			x +=4;
		if(x<0)
			x=0;
		if(x+120>maxWidth)
			x=maxWidth-120;
		
	}
	public void draw(Graphics g)
	{
		g.fillRect(x, y, width, height);
	}

}
