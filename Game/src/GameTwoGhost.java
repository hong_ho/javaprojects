import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;


public class GameTwoGhost implements Moveable
{//13-15
	int positionX, positionY,x,y,width,height;
	int collisionX=9, collisionY=9;// ghost ko rymma
	GameTwoPlayer target;
	
	long timeStamp;
	int time;
	
	final static int MODE_CONFUSE=0;//r�r sig random
	final static int MODE_AGGRESIV=1;// r�r sig aggretiv
	int mode;
	
	public GameTwoGhost(int posX, int posY, int t)
	{
		width=20;
		height=20;
		time=t*1000;// kolla time att byta mellan 2 r�relse
		
		positionX=posX;
		positionY=posY;
		
		mode=0;
		timeStamp=System.currentTimeMillis();
		
		
	}
	public void move()
	{
		long timeDiff=System.currentTimeMillis()-timeStamp;
		if(timeDiff>time)
		{
			switch(mode)
			{
			case 0:
				mode=1; break;
			case 1:
				mode=0; break;
			}
			timeStamp=System.currentTimeMillis();
		}
		if(timeDiff%1000<10)
		{
			selectPath();
		}
		positionX +=x;
		positionY +=y;
		
	}
	public void randomMove()
	{
		x=random();
		y=random();
		
	}
	private int random()
	{
		int rand=(int)(Math.round(Math.random()*2));
		if(rand==0)
			return 0;
		else if(rand==1)
			return 1;
		else
			return -1;
		
	}
	public void draw(Graphics g)
	{
		g.setColor(Color.orange);
		g.fillRect(positionX, positionY, width, height);
	}
	public boolean stillCollision() // ghost ko rymma
	{
		if(x==collisionX || y==collisionY )
		{
			return true;
		}
		return false;
	}
	public void selectPath()
	{
		if(mode==0)
		{
			randomMove();
		}
		else if(mode==1)
		{
			//target
		}
	}
	public void collision(GameTwoObject go) //ghost ko rymma
	{
		if(go instanceof GameTwoBlock)
		{
			collisionX=x;
			collisionY=y;
			do
			{
			selectPath();
			}
			while(stillCollision());
			collisionX=9;
			collisionY=9;
		}
	}
	@Override
	public Rectangle getBounds() {
		
		return new Rectangle(positionX, positionY, width, height);
	}
	
	
	
}
