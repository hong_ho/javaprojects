import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class SlantView extends JFrame implements Observer
{
	private JLabel status, kronor, klavar, hogkanter;
	private int kronCount, klavarCount, hogkantCount;
	private JButton knapp;
	public SlantView(SlantController SC)
	{
		kronCount = 0;
		klavarCount = 0;
		hogkantCount = 0;
		setSize(300, 100);
		setLayout(new GridLayout(3,2));
		doLayout(SC);
		pack();
		setTitle("Slantsingling");
		setVisible(true);
	}
	private void doLayout(SlantController SC)
	{
		status = new JLabel("");
		kronor = new JLabel("Krona: " + kronCount);
		klavar = new JLabel("Klave: " + klavarCount);
		hogkanter = new JLabel("H�gkant: " + hogkantCount);
		knapp = new JButton("Singla!");
		knapp.addActionListener(SC);
		add(status);
		add(knapp);
		add(kronor);
		add(klavar);
		add(hogkanter);
	}
	public void update(Observable arg0, Object arg1)
	{
		SlantModel.l�gen tempL�ge = (SlantModel.l�gen)arg1;
		if(tempL�ge == SlantModel.l�gen.KLAVE)
		{
			status.setText("Klave");
			klavar.setText("Klave: " + ++klavarCount);
		}
		else if(tempL�ge == SlantModel.l�gen.KRONA)
		{
			status.setText("Krona");
			kronor.setText("Krona: " + ++kronCount);
		}
		else
		{
			status.setText("H�gkant");
			hogkanter.setText("H�gkanter: " + ++hogkantCount);
		}
	}
}
