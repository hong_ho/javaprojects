import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;


public class GameTwoBlock implements GameTwoObject
{
	int width, height,x,y;
	
	public GameTwoBlock(int x, int y)
	{
		this.x=x;
		this.y=y;
		
		width=20;
		height=20;
	}
	public void draw(Graphics g)
	{
		g.setColor(Color.black);
		g.fillRect(x, y, width, height);
		//g.setColor(Color.red);
		//g.drawRect(x, y, width, height);
		
	}
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		//return null;
		return new Rectangle(x,y,width,height);
	}
	
}
