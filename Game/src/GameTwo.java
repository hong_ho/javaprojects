import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;


public class GameTwo extends JFrame implements KeyListener
{
	
	GameTwoPlayer hero;
	GameTwoMaze map;
	//GameTwoGhost  ghost;
	ArrayList<GameTwoGhost> ghosts;
	
	public static void main(String[] args)
	{ new GameTwo();}
	
	public GameTwo()
	{
		setSize(420,445);
		setVisible(true);
		setDefaultCloseOperation(3);
		//setBackground(Color.white);
		addKeyListener(this);
		
		//hero = new GameTwoPlayer(20, 20);
	//	ghost=new GameTwoGhost(20, 20, 120);
		map=new GameTwoMaze("maze.txt");
		gameLoop();
	}
	public void gameLoop()
	{
		try {
			map.createmaze();
		} catch (IOException e1) {
				e1.printStackTrace();
		}
		// bo o tren, them vao day khi co nhieu ghosts
		hero=map.getPlayer();
		ghosts=map.getGhosts();
		while(GameTwoState.going())
		{
			updateScreen();
			map.isCollision();
			//map.isCollision(ghost);//1ghost
			
			hero.move();
			//ghost.move();// 1 ghost
			
			for(GameTwoGhost g: ghosts)
			{
				g.move();
			}
			
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		endScreen();
	}
	public void endScreen()
	{
		Graphics g=getGraphics();
		g.drawString("GAME OVER", getWidth()/2-50, getHeight()/2);
	}
	public void updateScreen()
	{
		Image offScreen=createImage(getWidth(), getHeight());
		Graphics g=getGraphics();
		
		draw(offScreen.getGraphics());
		g.drawImage(offScreen,0,0,null);
		
	}
	public void draw(Graphics g)
	{
		for(GameTwoBlock b:map.getBlocks())
		{
			b.draw(g);
			//hero.collision(b);//1 ghost
		}
		hero.draw(g);
		//ghost.draw(g);//1 ghost
		
		for(GameTwoGhost ghost : ghosts)// nhieu ghosts
		{
			ghost.draw(g);
		}
	}
	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode=e.getKeyCode();
		if(keyCode==KeyEvent.VK_UP)
			hero.y =-5;
		if(keyCode==KeyEvent.VK_DOWN)
			hero.y =5;
		if(keyCode==KeyEvent.VK_LEFT)
			hero.x =-5;
		if(keyCode==KeyEvent.VK_RIGHT)
			hero.x =5;
		if(keyCode==KeyEvent.VK_SPACE)
			{
				hero.x =0;
				hero.y=0;
			}
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
