
public class SlantMain
{
	public static void main(String[] args)
	{
		SlantModel M = new SlantModel();
		SlantController C = new SlantController(M);
		SlantView V = new SlantView(C);
		M.addObserver(V);
	}
}
