import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.geom.Rectangle2D;


public class Ball 
{
	private int x=0, y=0, radius=15, dx=4,dy=4;
	private Color color=Color.blue;
	public Ball(int x, int y)
	{
		this.x=x;
		this.y=y;
	}
	public Ball(int x, int y, int radius, Color color)
	{
		this.x=x;
		this.y=y;
		this.radius=radius;
		this.color=color;
	}
	public void checkCollision(Block b)// kta de remove
	{ 
		Rectangle2D.Double self=new Rectangle2D.Double(x-radius,y-radius, radius*2, radius*2);
		Rectangle2D.Double block=new Rectangle2D.Double(b.x,b.y,b.width,b.height);
		if(self.intersects(block))
		{
			b.alive=false;
		}
	}
	public void move(Dimension dim, Paddle paddle)
	{
		x+=dx;
		y+=dy;
		
		// check tr�ffa 4 walls
					if(x<0)
						dx=Math.abs(dx);
					if(x> dim.width)
						dx=-Math.abs(dx);
					if(y<0)
						dy=Math.abs(dy);
					if(x>paddle.getX() && x < paddle.getX()+paddle.getW())
					{// GAP BAR
						if(y>=paddle.getY() && y<= paddle.getY()+paddle.getH())
							{
							dy=-Math.abs(dy);
							dy -=.2;
							if(dx < 0)
								dx -=.2;
							else
								dx += .2;
							}
					}
					if(y>dim.height+200)
					{// neu bar missa thi ko bi mat boll luon
						y=0;
					}
					
	}
	public void draw(Graphics g)
	{
		g.setColor(color);
		g.fillOval((int)(x-radius),(int)(y-radius), (int)(radius*2), (int)(radius*2));
	}
}
