import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class GameTwoMaze 
{
	ArrayList<GameTwoBlock> blocks;
	// khi them 3 ghost va 1 player
	ArrayList<GameTwoGhost> ghosts;
	ArrayList<GameTwoObject> go;
	ArrayList<Moveable> ma;
	
	GameTwoPlayer hero;
	String filePath;

	public GameTwoMaze(String path)
	{
		filePath=path;
		blocks=new ArrayList<GameTwoBlock>();
		ghosts=new ArrayList<GameTwoGhost>();
		go=new ArrayList<GameTwoObject>();
		ma=new ArrayList<Moveable>();
				
	}
	public void createmaze() throws IOException
	{
		ArrayList<String> lines=new ArrayList<String>();
		BufferedReader r=new BufferedReader(new FileReader(filePath));
		while(true)
		{
			String line=r.readLine();
			if(line==null)
			{
				r.close();
				break;
			}
			lines.add(line);
		}
		for(int y=0;y<lines.size();y++)// genom 20 rader
		{
			for(int x=0;x<lines.get(y).length();x++)// genom 20 stycker i en rad
			{
				char mark=lines.get(y).charAt(x);// get(y) kolla vilken rad, charat(x) vitri nao trong 1 rad
				
				if(mark=='#')
				{
					blocks.add(new GameTwoBlock(x*20+10, y*20+35));
					
				}
				else if(mark=='P')
				{
					hero=new GameTwoPlayer(x*20+10,y*20+35,20,20);
				}
				else if(mark=='C')
				{
					ghosts.add(new GameTwoGhost(x*20+10,y*20+35,40));
				}
			}
		}
		ma.addAll(ghosts);
		ma.add(hero);
		
		go.addAll(ma);
		go.addAll(blocks);
	}
	public ArrayList<GameTwoBlock> getBlocks()
	{
		return blocks;
	}
	public ArrayList<GameTwoGhost> getGhosts()
	{
		return ghosts;
	}
	public GameTwoPlayer getPlayer()
	{
		return hero;
	}
	/*public boolean isCollision(GameTwoGhost o)
	{
		for(GameTwoBlock b:getBlocks())
		{
		if(o.getBounds().intersects(b.getBounds()))
			{
				o.collision();
				return true;
			}
		}
		return false;
	}
	*/
	public void isCollision()
	{
		for(Moveable m : ma)
		{
			for(GameTwoObject o:go)
			{
				if(!m.equals(o)&&m.getBounds().intersects(o.getBounds()))
				{
					m.collision(o);
				}
			}
		}
		
	}
	
}
