package model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

public class ReadAndWriteShapes
{

	public void saveShapes(DrawingContainer v, String pathname)
	{
		try {
	        FileOutputStream fos = new FileOutputStream(pathname);
	        ObjectOutputStream oos = new ObjectOutputStream(fos);
	        oos.writeObject(v);
	        oos.flush();
	        oos.close();
	        fos.close();
	      } catch (Exception ex) {
	        System.out.println("Trouble writing display list vector");
	      }
	}
	public DrawingContainer readShapes(String pathname)
	{
		DrawingContainer displayList=new DrawingContainer();
		
		try {
	        FileInputStream fis = new FileInputStream(pathname);
	        ObjectInputStream ois = new ObjectInputStream(fis);
	        displayList = (DrawingContainer) (ois.readObject());
	        ois.close();
	        fis.close();
	        //repaint();
	      } catch (Exception ex) {
	        System.out.println("Trouble reading display list vector");
	      }
		return displayList;
	}
	public void exit()
	{
		System.exit(0);
	}
	
}
