package View;
import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.DrawingComposite;
import model.DrawingShape;

// TODO: Auto-generated Javadoc
/**
 * The Class DrawingUtil. This class holds the functionality for drawing on screen estate. Each method requires a valid
 * DrawingUtil and Graphics instances to work.
 */
public class DrawingUtil implements DrawingUtilInterface {
	
	
	/* (non-Javadoc)
	 * @see se.kau.isgc08.lab4.view.DrawingAPIInterface#drawCircle(se.kau.isgc08.lab4.model.DrawingShape)
	 */
	private Color lineColor;
	public void drawCircle(DrawingShape d, Graphics g) {
	
		if(d.isSelected()) {
			lineColor=Color.ORANGE;
		}
		else {
			lineColor=d.getLineColor();
		}
		if (d.getAreaColor()!=null) {
			GraphicsUtil.fillOval(g, d.getX1(), d.getY1(), d.getWidth(), d.getHeight(), d.getAreaColor());
		}
		GraphicsUtil.drawOval(g, d.getX1(), d.getY1(), d.getWidth(), d.getHeight(),
                d.getLineWidth(), lineColor);
	}
	
	/* (non-Javadoc)
	 * @see se.kau.isgc08.lab4.view.DrawingAPIInterface#drawRect(se.kau.isgc08.lab4.model.DrawingShape)
	 */
	public void drawRect(DrawingShape d,  Graphics g) {
		if(d.isSelected()) {
			lineColor=Color.ORANGE;
		}
		else {
			lineColor=d.getLineColor();
		}
		if (d.getAreaColor()!=null) {
			GraphicsUtil.fillRect(g, d.getX1(), d.getY1(), d.getWidth(), d.getHeight(), d.getAreaColor());
		}
		GraphicsUtil.drawRect(g, d.getX1(), d.getY1(), d.getWidth(), d.getHeight(),
                d.getLineWidth(), lineColor);
	}
	
	/* (non-Javadoc)
	 * @see se.kau.isgc08.lab4.view.DrawingAPIInterface#drawLine(se.kau.isgc08.lab4.model.DrawingShape)
	 */
	public void drawLine(DrawingShape d, Graphics g) {
		if(d.isSelected()) {
			lineColor=Color.ORANGE;
		}
		else {
			lineColor=d.getLineColor();
		}
		GraphicsUtil.drawLine(g, d.getX1(), d.getY1(), d.getWidth(), d.getHeight(),
                d.getLineWidth(),lineColor);
	}
	
	@Override
	public void setLocation(DrawingShape d,int x, int y) {
		// TODO Auto-generated method stub
		d.setLocation(d,x, y);
	}

}
