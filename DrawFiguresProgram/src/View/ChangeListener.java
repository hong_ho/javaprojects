package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ChangeListener implements ActionListener{
	private ViewShapes vs;
	public ChangeListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("change");
		
	}


}
