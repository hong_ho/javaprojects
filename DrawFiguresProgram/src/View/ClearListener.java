package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClearListener implements ActionListener{
	private ViewShapes vs;
	public ClearListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("cle");
		
	}

}
