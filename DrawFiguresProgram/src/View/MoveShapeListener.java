package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MoveShapeListener implements ActionListener{
	private ViewShapes vs;
	public MoveShapeListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("move");		
	}
}
