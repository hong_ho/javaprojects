package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CircleListener implements ActionListener {
	private ViewShapes vs;
	public CircleListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("cir");
		
	}
}
