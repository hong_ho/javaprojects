package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DeleteListener implements ActionListener {
	private ViewShapes vs;
	public DeleteListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("del");
		
	}

}
