package View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;

import controller.controler;
import model.Circle;
import model.DrawingContainer;
import model.DrawingShape;
import model.Line;
import model.Rect;




public class ViewShapes implements MouseListener, MouseMotionListener{
	private controler c;

	private JFrame j;
	private JMenuBar bar;
	private JMenu menu,edit;
	private JMenuItem save,load,close, clear, delete, moveSha, change;
	private DrawingContainer dc=new DrawingContainer();

	private JButton li, cir, rec, move;
	private DrawingPanel dp;
	private JComboBox<String> size,color,fill;
	private JLabel lbColor,lbFill,lbSize, lbMX,lbMY;
	private JTextField txtX,txtY;

	private int originX=0, originY=0, x,y;
	public boolean cirFlag=false;
	public boolean recFlag=false;
	public boolean lineFlag=false;
	public boolean moveFlag=false;

	private Color co=Color.BLACK;
	private Color fillColor=Color.WHITE;
	private DrawingShape ds;
	private Rect r;
	private Line l;
	private int w,h;
	private Circle circle;
	private int si=1;
	
	public ViewShapes(controler c){
		this.c=c;	
	}	
	public void setUp(DrawingContainer dc)
	{
		j=new JFrame("Drawing");	
		j.setSize(1000, 700); 
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		j.addWindowListener(new WinListenerExit(this));
		dp = new DrawingPanel(dc); 
		dp.setBackground(Color.WHITE);  

		j.add(dp);
		menu();
		tools();
		menuButtonListener();	

		dp.addMouseListener(this);
		dp.addMouseMotionListener(this);		

		j.setVisible(true);	
	}
	public void menu()
	{
		bar=new JMenuBar();
		menu=new JMenu("File");
		edit=new JMenu("Edit");
		save=new JMenuItem("Save");
		load=new JMenuItem("Load");
		close=new JMenuItem("Close");
		clear=new JMenuItem("Clear");
		delete=new JMenuItem("Delete");
		moveSha=new JMenuItem("Move");
		change=new JMenuItem("Change Size");
		menu.add(save);
		menu.add(load);
		menu.add(clear);
		menu.addSeparator();
		menu.add(close);

		edit.add(delete);
		edit.add(moveSha);
		edit.add(change);

		bar.add(menu);
		bar.add(edit);

		j.setJMenuBar(bar);
	}
	public void tools()
	{
		JPanel p=new JPanel();
		p.setBackground(Color.BLUE);
		li=new JButton(new ImageIcon("Pictures/line.png"));li.setBackground(Color.WHITE);
		cir=new JButton(new ImageIcon("Pictures/Circle.png"));cir.setBackground(Color.WHITE);
		rec=new JButton(new ImageIcon("Pictures/Rectangle.png"));rec.setBackground(Color.WHITE);
		move=new JButton(new ImageIcon("Pictures/Arrow.gif"));move.setBackground(Color.WHITE);

		String[] colors={"BLUE","WHITE","BLACK","PINK","GREEN"};
		String[] fillColors={"WHITE","BLACK","BLUE","PINK","GREEN"};
		String[] sizes={"1","2","3","4"};
		color=new JComboBox<>(colors);
		size=new JComboBox<>(sizes);
		fill=new JComboBox<>(fillColors);
		lbColor=new JLabel("LineColor ");lbColor.setForeground(Color.WHITE);
		lbSize=new JLabel("LineSize ");lbSize.setForeground(Color.WHITE);
		lbFill=new JLabel("FillColor ");lbFill.setForeground(Color.WHITE);
		lbMX=new JLabel("X");lbMX.setForeground(Color.WHITE);
		lbMY=new JLabel("Y");lbMY.setForeground(Color.WHITE);
		txtX=new JTextField(5);
		txtY=new JTextField(5);
		p.add(move);
		p.add(li);
		p.add(cir);
		p.add(rec);
		p.add(lbColor);
		p.add(color);
		p.add(lbSize);
		p.add(size);
		p.add(lbFill);
		p.add(fill);
		p.add(lbMX);
		p.add(txtX);
		p.add(lbMY);
		p.add(txtY);

		j.add(p,BorderLayout.NORTH);
		
	}
	public void menuButtonListener()
	{
		save.addActionListener(new SaveListener(this));
		load.addActionListener(new ReadListener(this));
		close.addActionListener(new CloseListener(this));
		clear.addActionListener(new ClearListener(this));
		delete.addActionListener(new DeleteListener(this));
		moveSha.addActionListener(new MoveShapeListener(this));
		change.addActionListener(new ChangeListener(this));

		move.addActionListener(new MoveListener(this));
		rec.addActionListener(new RectangleListener(this));
		li.addActionListener(new LineListener(this));
		cir.addActionListener(new CircleListener(this));
	}
	public void choose(String ch)
	{
		c.handleEvent(ch);
	}
	public void repaint()
	{
		dc.removeAll();		
		j.repaint();
	}

	public Color objLineColor()
	{
		if(color.getSelectedIndex()==0)
			co= Color.BLUE;
		if(color.getSelectedIndex()==1)
			co= Color.WHITE;
		if(color.getSelectedIndex()==2)
			co= Color.BLACK;
		if(color.getSelectedIndex()==3)
			co= Color.PINK;
		if(color.getSelectedIndex()==4)
			co= Color.GREEN;			
		return co;
	}	
	public Color setFillColor()
	{
		if(fill.getSelectedIndex()==0)
			fillColor= Color.WHITE;
		if(fill.getSelectedIndex()==1)
			fillColor= Color.BLACK;
		if(fill.getSelectedIndex()==2)
			fillColor= Color.BLUE;
		if(fill.getSelectedIndex()==3)
			fillColor= Color.PINK;
		if(fill.getSelectedIndex()==4)
			fillColor= Color.GREEN;			
		return fillColor;
	}
	
	public int setSize()
	{
		if(size.getSelectedIndex()==0)
			si=1;
		if(size.getSelectedIndex()==1)
			si=2;
		if(size.getSelectedIndex()==2)
			si=3;
		if(size.getSelectedIndex()==3)
			si=4;			
		return si;
	}
	public DrawingContainer newDC()
	{
		return dc;
	}

	// Called just after the user clicks the listened-to component.
	private int mX,mY;
	@Override
	public void mouseClicked(MouseEvent e) 
	{
		mX=e.getX();
		mY=e.getY();
		System.out.println("orgX1"+mX);
		System.out.println("orgY1"+mY);

	}
	// Called just after the cursor enters the bounds of the listened-to component.
	@Override
	public void mouseEntered(MouseEvent e) {

	}
	// Called just after the cursor exits the bounds of the listened-to component.
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub		
	}
	// Called just after the user presses a mouse button while the cursor is over the listened-to component.
	@Override
	public void mousePressed(MouseEvent e) {		
		originX = e.getX();
		originY = e.getY();	  	  
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		x = e.getX();
		y = e.getY();	
		System.out.println("X2"+x);
		System.out.println("y2"+y);
		if(x<originX && y<originY)
		{
			originX=x;
			originY=y;			
		}
		w=Math.abs(x-originX);
		h=Math.abs(y-originY);


		if(getRecFlag())
		{
			DrawingUtil da=new DrawingUtil();
			r=new Rect(da,originX,originY,w,h,setSize(),objLineColor(),setFillColor());
			//neu run objLineColor(); o constructor thi use co,si thay cho metoder
			//r=new Rect(da,originX,originY,w,h,si,co,setFillColor());
			
			dc.add(r);
			dp.setDc(dc);
			j.repaint();	

		}
		if(getCirFlag())
		{
			DrawingUtil da=new DrawingUtil();
			circle=new Circle(da,originX,originY,w,h,setSize(),objLineColor(),setFillColor());
			dc.add(circle);
			dp.setDc(dc);
			j.repaint();
		}
		if(getLineFlag())
		{
			DrawingUtil da=new DrawingUtil();
			Line l=new Line(da,originX,originY,x,y,setSize(),objLineColor());  
			dc.add(l);
			dp.setDc(dc);
			j.repaint();
		}

		if(getMoveFlag())
		{
			for(int i=0;i<dc.size();i++)
			{		
				if(originX>=dc.getEl(i).getX1() && originX<=dc.getEl(i).getX1()+dc.getEl(i).getWidth()
						&& originY>=dc.getEl(i).getY1()&& originY<=dc.getEl(i).getX1()+dc.getEl(i).getHeight())
				{					
					ds=(DrawingShape)dc.getEl(i);
					ds.setSelected(true);
					j.repaint();
				}
			}
		}			
	}
	// Called in response to the user moving the mouse while holding a mouse button down. 
	//This event is fired by the component that fired the most recent mouse-pressed event, 
	//even if the cursor is no longer over that component.
	@Override
	public void mouseDragged(MouseEvent e) {

		/*	if(getRecFlag())
		{
			w=e.getX()-originX;
			h=e.getY()-originY;

			r.setWidth(w);
			r.setHeight(h);
			j.repaint();
		}
		 */
		/*if(getRecFlag())
		{
		System.out.println("Mouse Dragged: ("+e.getX()+", "+e.getY() +")");
		w=e.getX()-originX;
		h=e.getY()-originY;
		DrawingUtil da=new DrawingUtil();
		r=new Rect(da,originX,originY,w,h,setSize(),setColor(),setFillColor());
		//Rect r=new Rect(da,89,89,120,120,3,Color.black,Color.green);
		dc.add(r);
		dp.setDc(dc);
		j.repaint();
		}
		if(getCirFlag())
		{
			originY=originX;
			w=e.getX()-originX;
			h=e.getY()-originY;
			DrawingUtil da=new DrawingUtil();
			Circle c=new Circle(da,originX,originY,w,h,setSize(),setColor(),setFillColor());
			dc.add(c);
			dp.setDc(dc);
			j.repaint();
		}
		 */
	}
	// Called in response to the user moving the mouse with no mouse buttons pressed. 
	//This event is fired by the component that's currently under the cursor.
	@Override
	public void mouseMoved(MouseEvent e) {
		//System.out.println("Mouse Moved: ("+e.getX()+", "+e.getY() +")");

	}
	
	public DrawingContainer vectorShapes()
	{
		return dc;
	}
	public void readShapes(DrawingContainer dc)
	{
		this.dc=dc;
		dp.setDc(dc);
		j.repaint();
	}

	public void setCirFlag(boolean f)
	{
		this.cirFlag=f;
	}
	public boolean getCirFlag()
	{
		return cirFlag;
	}
	public void setRecFlag(boolean f)
	{
		this.recFlag=f;
	}
	public boolean getRecFlag()
	{
		return recFlag;
	}
	public void setLineFlag(boolean f)
	{
		this.lineFlag=f;
	}
	public boolean getLineFlag()
	{
		return lineFlag;
	}
	public void setMoveFlag(boolean f)
	{
		this.moveFlag=f;
	}
	public boolean getMoveFlag()
	{
		return moveFlag;
	}
	public void delete()
	{
		dc.remove(ds);
		j.repaint();

	}
	public void move()
	{
		if(txtX.getText().equals("")||txtY.getText().equals(""))
		{
			JOptionPane.showMessageDialog(null, "mata in x och y:");
		}
		else
		{
			int z=Integer.parseInt(txtX.getText());
			int h=Integer.parseInt(txtY.getText());

			ds.setSelected(false);

			String s=ds.toString();
			if(s.contains("Line"))
			{
				if(z<ds.getX1())
				{
					ds.setWidth(ds.getWidth()-z);
					ds.setHeight(ds.getHeight()-h);
					ds.setX1(z);
					ds.setY1(h);
				}
				else
				{
					ds.setWidth(ds.getX1()-ds.getWidth()+z);
					ds.setHeight(ds.getY1()-ds.getHeight()+h);
					ds.setX1(z);
					ds.setY1(h);
				}
			}
			else
			{
				ds.setX1(z);
				ds.setY1(h);
			}
			j.repaint();
		}	
	}
	public void changeSize()
	{
		int w=Integer.parseInt(txtX.getText());
		int h=Integer.parseInt(txtY.getText());

		ds.setSelected(false);
		ds.setWidth(w);
		ds.setHeight(h);
		j.repaint();
	}


}
