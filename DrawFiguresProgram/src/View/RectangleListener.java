package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class RectangleListener implements ActionListener{
	private ViewShapes vs;
	public RectangleListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("rec");
		
	}
}

