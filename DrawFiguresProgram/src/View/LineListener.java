package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LineListener implements ActionListener{
	private ViewShapes vs;
	public LineListener(ViewShapes vs) {
		this.vs=vs;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		vs.choose("li");
		
	}
}
