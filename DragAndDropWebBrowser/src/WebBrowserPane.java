import java.net.*;
import java.util.*;
import java.io.*;
import javax.swing.*;

import javax.swing.JEditorPane;

public class WebBrowserPane extends JEditorPane 
{
	private List history=new ArrayList();
	private int historyIndex;
	//{}
	public WebBrowserPane()	
	{
		setEditable(false);
	}	
	public void gotoURL(URL url)
	{
		displayPage(url);
		history.add(url);
		historyIndex=history.size()-1;
	}
	private void displayPage(URL pageUrl) 
	{
		try
		{
			setPage(pageUrl);
		}
		catch(IOException ioEx)
		{
			ioEx.printStackTrace();
		}		
	}
	public URL forward()
	{
		historyIndex++;
		if(historyIndex>=history.size())
			historyIndex=history.size()-1;
		
		URL url=(URL)history.get(historyIndex);
		displayPage(url);
		
		return url;
	}
	public URL back()
	{
		historyIndex--;
		if(historyIndex<0)
			historyIndex=0;
		
		URL url=(URL)history.get(historyIndex);
		displayPage(url);
		
		return url;
	}
}
