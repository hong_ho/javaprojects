package com.deitel.advjhtp1.gui.actions;
import java.awt.event.*;
import javax.swing.*;


public abstract class MyAbstractAction extends AbstractAction
{
	public MyAbstractAction() {
		// TODO Auto-generated constructor stub
	}
	public MyAbstractAction(String name,Icon icon, String description, Integer mnemonic)
	{
		setName(name);
		setSmallIcon(icon);
		setShortDescription(description);
		stMnemonic(mnemonic);
	}
	private void stMnemonic(Integer mnemonic) {
		putValue(Action.MNEMONIC_KEY, mnemonic);
	}
	private void setShortDescription(String description) {
		putValue(Action.SHORT_DESCRIPTION, description);
	}
	private void setSmallIcon(Icon icon) {
		putValue(Action.SMALL_ICON, icon);
	}
	private void setName(String name) {
		putValue(Action.NAME, name);
		
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
