
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.*;
import javax.swing.event.*;


import DragAndDropWebBrowser.MyAbstractAction;
public class WebToolBar extends JToolBar implements HyperlinkListener
{
	private WebBrowserPane webBrowser;
	//private JButton backBtn,forwardBtn;
	private JTextField urlTextField;
	
	
	public WebToolBar(WebBrowserPane wb, Locale locale)
	{
		// get resource bundle for internationalized strings
		
		ResourceBundle resource=ResourceBundle.getBundle("StringsAndLabels",locale);
		
		setName(resource.getString("toolBarTitle"));
				
		//Register for HyperlinkEvents
		webBrowser=wb;
		webBrowser.addHyperlinkListener(this);
		
		//create JTextField for entering URLs
		urlTextField=new JTextField(25);
		urlTextField.addActionListener(new ActionListener(){
			//navigate wb to user-entered url
			public void actionPerformed(ActionEvent e) {
				try
				{// attempt to load url in webbrowserpane
					URL url=new URL(urlTextField.getText());
					webBrowser.gotoURL(url);
				}
				catch(MalformedURLException urlEx)
				{
					urlEx.printStackTrace();
				}
			}
		});
		
		//create backaction and configure its properties
		MyAbstractAction backAction=new MyAbstractAction()
		{
			
			public void actionPerformed(ActionEvent e) {
				//navigate to previous url
				URL url=webBrowser.back();
				//display url in textfield
				urlTextField.setText(url.toString());
				
			}
		};
		backAction.setSmallIcon(new ImageIcon(getClass().getResource("images/back.gif")));
		
		//create btn for navigating to next history url
				//backBtn=new JButton(new ImageIcon(getClass().getResource("images/back.gif")));
				forwardBtn=new JButton(">>");
				forwardBtn.addActionListener(new ActionListener() {	
					@Override
					public void actionPerformed(ActionEvent e) {
						//navigate to previous url
						URL url=webBrowser.forward();
						//display url in textfield
						urlTextField.setText(url.toString());
						
					}
				});
				
				
				add(backBtn);
				add(forwardBtn);
				add(urlTextField);
	}


	@Override
	public void hyperlinkUpdate(HyperlinkEvent event) 
	{//if hyperlink was activated, go to hyperlink's url
		if(event.getEventType()==HyperlinkEvent.EventType.ACTIVATED)
		{
			//get URL from HyperlinkEvent
			URL url=event.getURL();
			
			//navigate to URL and display URL in textField
			webBrowser.gotoURL(url);
			urlTextField.setText(url.toString());
			
		}
		
	}
	
}
