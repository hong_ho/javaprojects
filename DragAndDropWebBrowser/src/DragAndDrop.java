import javax.swing.*;
import java.net.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.io.*;
import java.util.*;
import java.util.List;


public class DragAndDrop extends JFrame
{
	private WebToolBar toolBar;
	private WebBrowserPane browserPane;
	
	public DragAndDrop()
	{
		super("Drag and Drop Web Browser");
		browserPane=new WebBrowserPane();
		toolBar=new WebToolBar(browserPane);
		
		browserPane.setDropTarget(new DropTarget(browserPane, 
				DnDConstants.ACTION_COPY, new DropTargetHandler()));
		
		Container contentPane=getContentPane();
		contentPane.add(toolBar, BorderLayout.NORTH);
		contentPane.add(new JScrollPane(browserPane),BorderLayout.CENTER);
		
	}
	//inner class to handle DropTargetEvents
	private class DropTargetHandler implements DropTargetListener
	{

		@Override
		public void dragEnter(DropTargetDragEvent event) {
			if(event.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
				event.acceptDrag(DnDConstants.ACTION_COPY);
			else
				event.rejectDrag();
			
		}

		@Override
		public void dragExit(DropTargetEvent dte) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dragOver(DropTargetDragEvent dtde) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void drop(DropTargetDropEvent event) {
			Transferable transferable=event.getTransferable();
			if(transferable.isDataFlavorSupported(DataFlavor.javaFileListFlavor))
			{
				event.acceptDrop(DnDConstants.ACTION_COPY);
				try
				{
					List fileList=(List)transferable.getTransferData(DataFlavor.javaFileListFlavor);
					
					Iterator iterator=fileList.iterator();
					
					while(iterator.hasNext())
					{
						File file=(File)iterator.next();
						
						browserPane.gotoURL(file.toURL());
						
					}
					event.dropComplete(true);

				}
				catch(UnsupportedFlavorException flEx)
				{
					flEx.printStackTrace();
					event.dropComplete(false);
				}
				catch( IOException ioEx)
				{
					ioEx.printStackTrace();
					event.dropComplete(false);
				}

			}
			else
				event.rejectDrop();
			
		}

		@Override
		public void dropActionChanged(DropTargetDragEvent dtde) {
			// TODO Auto-generated method stub
			
		}
		

	}
	public static void main(String[] args) 
	{
		DragAndDrop dAd=new DragAndDrop();
		dAd.setDefaultCloseOperation(EXIT_ON_CLOSE);
		dAd.setSize(640,480);
		dAd.setVisible(true);

	}

}
